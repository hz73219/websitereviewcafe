﻿namespace WebsiteReviewCafe.Models
{
    public class AddPost
    {
        public string Title { get; set; } = null!;

        public string ContentPost { get; set; } = null!;
        public int IdTag { get; set; }
        public string NameShop { get; set; } = null!;

        public string DiaChi { get; set; } = null!;

        public TimeSpan ClosingTime { get; set; }

        public TimeSpan OpenTime { get; set; }

        public string? PhoneShop { get; set; }

        public string? NameShopOwner { get; set; }
        public List<string> nameDrink { get; set; }
        public List<int> priceDrink { get; set; }
    }
}
