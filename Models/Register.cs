﻿namespace WebsiteReviewCafe.Models
{
    public class Register
    {
        public string UserName { get; set; } = null!;

        public string Password { get; set; } = null!;
        public string FullName { get; set; } = null!;

        public DateTime Birthday { get; set; }

        public string PhoneNumber { get; set; } = null!;

        public string Email { get; set; } = null!;
    }
}
