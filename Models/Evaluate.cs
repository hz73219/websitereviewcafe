﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace WebsiteReviewCafe.Models;

public partial class Evaluate
{
    public int IdEvaluate { get; set; }

    public int IdPost { get; set; }

    public int UserId { get; set; }

    public int Star { get; set; }
    [JsonIgnore]
    public virtual Post IdPostNavigation { get; set; } = null!;

    public virtual User User { get; set; } = null!;
}
