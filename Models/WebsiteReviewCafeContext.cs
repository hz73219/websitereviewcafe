﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace WebsiteReviewCafe.Models;

public partial class WebsiteReviewCafeContext : DbContext
{
    public WebsiteReviewCafeContext()
    {
    }

    public WebsiteReviewCafeContext(DbContextOptions<WebsiteReviewCafeContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Account> Accounts { get; set; }

    public virtual DbSet<Comment> Comments { get; set; }

    public virtual DbSet<Evaluate> Evaluates { get; set; }

    public virtual DbSet<Menu> Menus { get; set; }

    public virtual DbSet<Post> Posts { get; set; }

    public virtual DbSet<PostAdvertisement> PostAdvertisements { get; set; }

    public virtual DbSet<PostPin> PostPins { get; set; }

    public virtual DbSet<Shop> Shops { get; set; }

    public virtual DbSet<Tag> Tags { get; set; }

    public virtual DbSet<Token> Tokens { get; set; }

    public virtual DbSet<UpgradeAccount> UpgradeAccounts { get; set; }

    public virtual DbSet<User> Users { get; set; }

    public virtual DbSet<Visit> Visits { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("Server= LAPTOP-6791LU43;Database= Website_Review_Cafe; user id=sa;password=12;Trusted_Connection=True;TrustServerCertificate=True\n");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Account>(entity =>
        {
            entity.HasKey(e => e.IdAccount);

            entity.ToTable("Account");

            entity.Property(e => e.IdAccount).HasColumnName("idAccount");
            entity.Property(e => e.Password)
                .HasMaxLength(200)
                .IsUnicode(false)
                .HasColumnName("password");
            entity.Property(e => e.Role)
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasColumnName("role");
            entity.Property(e => e.UserId).HasColumnName("userId");
            entity.Property(e => e.UserName)
                .HasMaxLength(20)
                .IsUnicode(false)
                .HasColumnName("userName");

            entity.HasOne(d => d.User).WithMany(p => p.Accounts)
                .HasForeignKey(d => d.UserId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Account_User");
        });

        modelBuilder.Entity<Comment>(entity =>
        {
            entity.HasKey(e => e.IdComment);

            entity.ToTable("Comment");

            entity.Property(e => e.IdComment).HasColumnName("idComment");
            entity.Property(e => e.Comment1)
                .HasMaxLength(200)
                .HasColumnName("comment");
            entity.Property(e => e.date)
                .HasColumnType("datetime")
                .HasColumnName("date");
            entity.Property(e => e.IdPost).HasColumnName("idPost");
            entity.Property(e => e.IdUser).HasColumnName("idUser");
            entity.Property(e => e.Image)
                .HasMaxLength(40)
                .IsFixedLength()
                .HasColumnName("image");
        });

        modelBuilder.Entity<Evaluate>(entity =>
        {
            entity.HasKey(e => e.IdEvaluate);

            entity.ToTable("Evaluate");

            entity.Property(e => e.IdEvaluate).HasColumnName("idEvaluate");
            entity.Property(e => e.IdPost).HasColumnName("idPost");
            entity.Property(e => e.Star).HasColumnName("star");
            entity.Property(e => e.UserId).HasColumnName("userId");

            entity.HasOne(d => d.IdPostNavigation).WithMany(p => p.Evaluates)
                .HasForeignKey(d => d.IdPost)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Evaluate_Post");

            entity.HasOne(d => d.User).WithMany(p => p.Evaluates)
                .HasForeignKey(d => d.UserId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Evaluate_User");
        });

        modelBuilder.Entity<Menu>(entity =>
        {
            entity.HasKey(e => e.IdDrink);

            entity.ToTable("Menu");

            entity.Property(e => e.IdDrink).HasColumnName("idDrink");
            entity.Property(e => e.IdShop).HasColumnName("idShop");
            entity.Property(e => e.NameDrink)
                .HasMaxLength(100)
                .HasColumnName("nameDrink");
            entity.Property(e => e.Price).HasColumnName("price");

            entity.HasOne(d => d.IdShopNavigation).WithMany(p => p.Menus)
                .HasForeignKey(d => d.IdShop)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Menu_Shop");
        });

        modelBuilder.Entity<Post>(entity =>
        {
            entity.HasKey(e => e.IdPost);

            entity.ToTable("Post");

            entity.Property(e => e.IdPost).HasColumnName("idPost");
            entity.Property(e => e.ContentPost).HasColumnName("contentPost");
            entity.Property(e => e.Date)
                .HasColumnType("datetime")
                .HasColumnName("date");
            entity.Property(e => e.IdShop).HasColumnName("idShop");
            entity.Property(e => e.IdTag).HasColumnName("idTag");
            entity.Property(e => e.Image)
                .HasMaxLength(60)
                .IsFixedLength()
                .HasColumnName("image");
            entity.Property(e => e.Status).HasColumnName("status");
            entity.Property(e => e.Title)
                .HasMaxLength(200)
                .HasColumnName("title");
            entity.Property(e => e.UserId).HasColumnName("userId");

            entity.HasOne(d => d.IdShopNavigation).WithMany(p => p.Posts)
                .HasForeignKey(d => d.IdShop)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Post_Shop");

            entity.HasOne(d => d.IdTagNavigation).WithMany(p => p.Posts)
                .HasForeignKey(d => d.IdTag)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Post_Tag");

            entity.HasOne(d => d.User).WithMany(p => p.Posts)
                .HasForeignKey(d => d.UserId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Post_User");
        });

        modelBuilder.Entity<PostAdvertisement>(entity =>
        {
            entity.HasKey(e => e.IdPostAdv);

            entity.ToTable("PostAdvertisement");

            entity.Property(e => e.IdPostAdv).HasColumnName("idPostAdv");
            entity.Property(e => e.IdPost).HasColumnName("idPost");
            entity.Property(e => e.IdUser).HasColumnName("idUser");
            entity.Property(e => e.views).HasColumnName("views");
            entity.HasOne(d => d.IdPostNavigation).WithMany(p => p.PostAdvertisements)
                .HasForeignKey(d => d.IdPost)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_PostAdvertisement_Post");

            entity.HasOne(d => d.IdUserNavigation).WithMany(p => p.PostAdvertisements)
                .HasForeignKey(d => d.IdUser)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_PostAdvertisement_User");
        });

        modelBuilder.Entity<PostPin>(entity =>
        {
            entity.HasKey(e => e.IdPostPin);

            entity.ToTable("PostPin");

            entity.Property(e => e.IdPost).HasColumnName("idPost");

            entity.HasOne(d => d.IdPostNavigation).WithMany(p => p.PostPins)
                .HasForeignKey(d => d.IdPost)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_PostPin_Post");
        });

        modelBuilder.Entity<Shop>(entity =>
        {
            entity.HasKey(e => e.IdShop);

            entity.ToTable("Shop");

            entity.Property(e => e.IdShop).HasColumnName("idShop");
            entity.Property(e => e.ClosingTime)
                .HasPrecision(0)
                .HasColumnName("closingTime");
            entity.Property(e => e.DiaChi)
                .HasMaxLength(200)
                .HasColumnName("diaChi");
            entity.Property(e => e.NameShop)
                .HasMaxLength(100)
                .HasColumnName("nameShop");
            entity.Property(e => e.NameShopOwner)
                .HasMaxLength(50)
                .HasColumnName("nameShopOwner");
            entity.Property(e => e.OpenTime)
                .HasPrecision(0)
                .HasColumnName("openTime");
            entity.Property(e => e.PhoneShop)
                .HasMaxLength(11)
                .IsUnicode(false)
                .IsFixedLength()
                .HasColumnName("phoneShop");
        });

        modelBuilder.Entity<Tag>(entity =>
        {
            entity.HasKey(e => e.IdTag);

            entity.ToTable("Tag");

            entity.Property(e => e.IdTag).HasColumnName("idTag");
            entity.Property(e => e.NameTag)
                .HasMaxLength(20)
                .HasColumnName("nameTag");
        });

        modelBuilder.Entity<Token>(entity =>
        {
            entity.HasKey(e => e.IdToken);

            entity.ToTable("Token");

            entity.Property(e => e.IdToken).HasColumnName("idToken");
            entity.Property(e => e.DateExpired)
                .HasColumnType("date")
                .HasColumnName("dateExpired");
            entity.Property(e => e.Token1)
                .HasMaxLength(10)
                .IsUnicode(false)
                .IsFixedLength()
                .HasColumnName("token");
            entity.Property(e => e.UserId).HasColumnName("userId");

            entity.HasOne(d => d.User).WithMany(p => p.Tokens)
                .HasForeignKey(d => d.UserId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Token_User");
        });

        modelBuilder.Entity<UpgradeAccount>(entity =>
        {
            entity.HasKey(e => e.IUpgrade);

            entity.ToTable("UpgradeAccount");

            entity.Property(e => e.IUpgrade).HasColumnName("iUpgrade");
            entity.Property(e => e.IdAccount).HasColumnName("idAccount");
            entity.Property(e => e.TimeUpgrade)
                .HasColumnType("date")
                .HasColumnName("timeUpgrade");

            entity.HasOne(d => d.IdAccountNavigation).WithMany(p => p.UpgradeAccounts)
                .HasForeignKey(d => d.IdAccount)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_UpgradeAccount_Account");
        });

        modelBuilder.Entity<User>(entity =>
        {
            entity.ToTable("User");

            entity.Property(e => e.UserId).HasColumnName("userId");
            entity.Property(e => e.Birthday)
                .HasColumnType("date")
                .HasColumnName("birthday");
            entity.Property(e => e.Email)
                .HasMaxLength(25)
                .IsFixedLength()
                .HasColumnName("email");
            entity.Property(e => e.FullName)
                .HasMaxLength(70)
                .HasColumnName("fullName");
            entity.Property(e => e.Image)
                .HasMaxLength(50)
                .HasColumnName("image");
            entity.Property(e => e.PhoneNumber)
                .HasMaxLength(10)
                .IsUnicode(false)
                .IsFixedLength()
                .HasColumnName("phoneNumber");
        });

        modelBuilder.Entity<Visit>(entity =>
        {
            entity.HasKey(e => e.IdVisit);

            entity.ToTable("Visit");

            entity.Property(e => e.IdVisit).HasColumnName("idVisit");
            entity.Property(e => e.IdPost).HasColumnName("idPost");
            entity.Property(e => e.IdUser).HasColumnName("idUser");
            entity.Property(e => e.Visit1).HasColumnName("visit");
            entity.Property(e => e.date)
                .HasColumnType("date")
                .HasColumnName("date");
            entity.HasOne(d => d.IdPostNavigation).WithMany(p => p.Visits)
                .HasForeignKey(d => d.IdPost)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Visit_Post");

            entity.HasOne(d => d.IdUserNavigation).WithMany(p => p.Visits)
                .HasForeignKey(d => d.IdUser)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Visit_User");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
