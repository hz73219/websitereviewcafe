﻿using System;
using System.Collections.Generic;

namespace WebsiteReviewCafe.Models;

public partial class Visit
{
    public int IdVisit { get; set; }

    public int IdPost { get; set; }

    public int IdUser { get; set; }

    public int Visit1 { get; set; }
    public DateTime date { get; set; }

    public virtual Post IdPostNavigation { get; set; } = null!;

    public virtual User IdUserNavigation { get; set; } = null!;
}
