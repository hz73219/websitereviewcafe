﻿using System;
using System.Collections.Generic;

namespace WebsiteReviewCafe.Models;

public partial class User
{
    public int UserId { get; set; }

    public string FullName { get; set; } = null!;

    public DateTime Birthday { get; set; }

    public string PhoneNumber { get; set; } = null!;

    public string Email { get; set; } = null!;

    public string? Image { get; set; }

    public virtual ICollection<Account> Accounts { get; } = new List<Account>();

    public virtual ICollection<Evaluate> Evaluates { get; } = new List<Evaluate>();

    public virtual ICollection<PostAdvertisement> PostAdvertisements { get; } = new List<PostAdvertisement>();

    public virtual ICollection<Post> Posts { get; } = new List<Post>();

    public virtual ICollection<Token> Tokens { get; } = new List<Token>();

    public virtual ICollection<Visit> Visits { get; } = new List<Visit>();

}
