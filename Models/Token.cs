﻿using System;
using System.Collections.Generic;

namespace WebsiteReviewCafe.Models;

public partial class Token
{
    public int IdToken { get; set; }

    public int UserId { get; set; }

    public DateTime DateExpired { get; set; }

    public string Token1 { get; set; } = null!;

    public virtual User User { get; set; } = null!;
}
