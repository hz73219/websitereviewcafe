﻿using System;
using System.Collections.Generic;

namespace WebsiteReviewCafe.Models;

public partial class Tag
{
    public int IdTag { get; set; }

    public string NameTag { get; set; } = null!;

    public virtual ICollection<Post> Posts { get; } = new List<Post>();
}
