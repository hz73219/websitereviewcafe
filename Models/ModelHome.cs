﻿namespace WebsiteReviewCafe.Models
{
    public class ModelHome
    {
        public List<Post> listPostPin { get; set; }
        public List<Post> listPostTrending { get; set; }
        public List<Post> listPostMostView { get; set; }
        public List<Post> listPostNew { get; set; }
        public List<Post> listPostADS { get; set; }
        public List<Tag> listTag { get; set; }
        public List<Post> listPost { get; set; }
        public string message { get; set; } 
    }
}
