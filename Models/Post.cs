﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace WebsiteReviewCafe.Models;

public partial class Post
{
    public int IdPost { get; set; }

    public string Title { get; set; } = null!;

    public string ContentPost { get; set; } = null!;

    public int IdShop { get; set; }

    public bool Status { get; set; }

    public DateTime Date { get; set; }

    public int IdTag { get; set; }

    public int UserId { get; set; }

    public string Image { get; set; } = null!;

    public virtual ICollection<Evaluate> Evaluates { get; } = new List<Evaluate>();
    [JsonIgnore]
    public virtual Shop IdShopNavigation { get; set; } = null!;

    public virtual Tag IdTagNavigation { get; set; } = null!;

    public virtual ICollection<PostAdvertisement> PostAdvertisements { get; } = new List<PostAdvertisement>();

    public virtual ICollection<PostPin> PostPins { get; } = new List<PostPin>();

    public virtual User User { get; set; } = null!;
    public virtual ICollection<Visit> Visits { get; } = new List<Visit>();
}
