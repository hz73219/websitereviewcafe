﻿using System;
using System.Collections.Generic;

namespace WebsiteReviewCafe.Models;

public partial class Comment
{
    public int IdComment { get; set; }

    public int IdPost { get; set; }

    public int IdUser { get; set; }
    public DateTime date { get; set; }

    public string Comment1 { get; set; } = null!;

    public string? Image { get; set; }

}
