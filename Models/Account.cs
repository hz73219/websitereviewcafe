﻿using System;
using System.Collections.Generic;

namespace WebsiteReviewCafe.Models;

public partial class Account
{
    public int IdAccount { get; set; }

    public string UserName { get; set; } = null!;

    public string Password { get; set; } = null!;

    public string Role { get; set; } = null!;

    public int UserId { get; set; }

    public virtual ICollection<UpgradeAccount> UpgradeAccounts { get; } = new List<UpgradeAccount>();

    public virtual User User { get; set; } = null!;
}
