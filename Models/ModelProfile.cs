﻿using System.Collections.Generic;
using System.Net;

namespace WebsiteReviewCafe.Models
{
    public class ModelProfile
    {
        public User User { get; set; }
        public List<double> Evaluate { get; set; }
        public List<Post> Post { get; set; }
        public List<Shop> Shop { get; set; }
        public bool ads { get; set; } = false;
        public UpgradeAccount UpgradeAccount { get; set; }
        public ModelProfile()
        {
            Shop = new List<Shop>();
            Evaluate = new List<double>();
        }
    }
}
