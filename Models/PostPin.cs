﻿using System;
using System.Collections.Generic;

namespace WebsiteReviewCafe.Models;

public partial class PostPin
{
    public int IdPostPin { get; set; }

    public int IdPost { get; set; }

    public virtual Post IdPostNavigation { get; set; } = null!;
}
