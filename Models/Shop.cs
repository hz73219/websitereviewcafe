﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace WebsiteReviewCafe.Models;

public partial class Shop
{
    public int IdShop { get; set; }

    public string NameShop { get; set; } = null!;

    public string DiaChi { get; set; } = null!;

    public TimeSpan ClosingTime { get; set; }

    public TimeSpan OpenTime { get; set; }

    public string? PhoneShop { get; set; }

    public string? NameShopOwner { get; set; }

    public virtual ICollection<Menu> Menus { get; } = new List<Menu>();
    [JsonIgnore]
    public virtual ICollection<Post> Posts { get; } = new List<Post>();
}
