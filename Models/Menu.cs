﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace WebsiteReviewCafe.Models;

public partial class Menu
{
    public int IdDrink { get; set; }

    public int IdShop { get; set; }

    public string NameDrink { get; set; } = null!;

    public int Price { get; set; }
    [JsonIgnore]
    public virtual Shop IdShopNavigation { get; set; } = null!;
}
