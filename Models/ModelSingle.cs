﻿namespace WebsiteReviewCafe.Models
{
    public class ModelSingle
    {
        public User User { get; set; }
        public Shop Shop { get; set; }
        public Post Post { get; set; }
        public List<Comment> ListComment { get; set; }
        public List<User> ListUserComment { get; set; }
        public int star { get; set; } = 5;
        public int reviews { get; set; } = 0;
        public List<Tag> listTag { get; set; }
        public List<Post> listPostTrending { get; set; }
        public List<Post> listPostMostView { get; set; }
    }
}
