﻿using System;
using System.Collections.Generic;

namespace WebsiteReviewCafe.Models;

public partial class PostAdvertisement
{
    public int IdPostAdv { get; set; }

    public int IdPost { get; set; }

    public int IdUser { get; set; }
    public int views { get; set; }
    public virtual Post IdPostNavigation { get; set; } = null!;

    public virtual User IdUserNavigation { get; set; } = null!;
}
