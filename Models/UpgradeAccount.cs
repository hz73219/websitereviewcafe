﻿using System;
using System.Collections.Generic;

namespace WebsiteReviewCafe.Models;

public partial class UpgradeAccount
{
    public int IUpgrade { get; set; }

    public int IdAccount { get; set; }

    public DateTime TimeUpgrade { get; set; }

    public virtual Account IdAccountNavigation { get; set; } = null!;
}
