﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebsiteReviewCafe.Models;

namespace WebsiteReviewCafe.Controllers
{
    public class SingleController : BaseController
    {
        WebsiteReviewCafeContext db = new WebsiteReviewCafeContext();
        public IActionResult Index(int id)
        {
            int accountId = int.Parse(getDataToken());
            int idUser = db.Accounts.Find(accountId).UserId;
            if (null == db.Posts.Find(id))
                { return RedirectToAction("Index", "Home", new { }); }
            var visit = db.Visits.Where(x=>x.IdUser == idUser&&x.IdPost==id).FirstOrDefault();
            if (null== visit)
            {
                Visit visitAdd = new Visit();
                visitAdd.IdUser = idUser;
                visitAdd.IdPost = id;
                visitAdd.date = DateTime.Now;
                visitAdd.Visit1 = 1;
                db.Visits.Add(visitAdd);
                db.SaveChanges();
            }
            else if(visit.date<DateTime.Today)
            {
                visit.date = DateTime.Today;
                visit.Visit1++;
                db.SaveChanges();
            }
            
            ModelSingle modelSingle = new ModelSingle();
            var listRatting = db.Evaluates.Where(r => r.IdPost == id).ToList();
            if (listRatting.Count!=0)
            {
                int ratting = (int)(listRatting.Average(r => r.Star));
                modelSingle.star = ratting;
                modelSingle.reviews = listRatting.Count;
            }       
            modelSingle.ListComment = db.Comments.Where(x=>x.IdPost==id).OrderByDescending(x=>x.date).ToList();
            modelSingle.ListUserComment = new List<User>();
            foreach (var x in modelSingle.ListComment)
            {
                modelSingle.ListUserComment.Add(db.Users.Find(x.IdUser));
            }           
            modelSingle.Post = db.Posts.Find(id);
            modelSingle.User = db.Users.Where(x => x.UserId == modelSingle.Post.UserId).FirstOrDefault();
            modelSingle.Shop = db.Shops.Where(x => x.IdShop == modelSingle.Post.IdShop).Include(x=>x.Menus).FirstOrDefault();
            modelSingle.listPostMostView = getListPostMostView();
            modelSingle.listPostTrending = getListPostAds();
            modelSingle.listTag = db.Tags.ToList();
            return View(modelSingle);
        }
        [HttpPost]
        public IActionResult addComment(int id,string comment, IFormFile image)
        {
            int accountId = int.Parse(getDataToken());
            int idUser = db.Accounts.Find(accountId).UserId;
            if (string.IsNullOrEmpty(comment))
            {
                return RedirectToAction("Index", "Single", new { id = id });
            }
            Comment commentAdd = new Comment();
            commentAdd.Comment1 = comment;
            commentAdd.IdPost = id;
            commentAdd.IdUser = idUser;
            commentAdd.date = DateTime.Now;
            if (null!= image)
            {
                var fileName = GenerateRandomString(12) + ".png";
                var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "images", "comment", fileName);
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    image.CopyTo(stream);
                }
                commentAdd.Image = "/Images/comment/" + fileName;
            }
            db.Comments.Add(commentAdd);
            db.SaveChanges();
            return RedirectToAction("Index", "Single", new {id =id });
        }
        
        [HttpPost]
        public IActionResult rate(int rating ,int id)
        {
            int accountId = int.Parse(getDataToken());
            int idUser = db.Accounts.Find(accountId).UserId;
            var rate = db.Evaluates.Where(x => x.IdPost == id && x.UserId == idUser).FirstOrDefault();
            if( null == rate)
            {
                Evaluate evaluate = new Evaluate();
                evaluate.IdPost = id;
                evaluate.UserId = idUser;
                evaluate.Star = rating;
                db.Evaluates.Add(evaluate);
                db.SaveChanges();
                return RedirectToAction("Index", "Single", new { id = id });

            }
            rate.Star = rating;
            db.SaveChanges();
            return RedirectToAction("Index", "Single", new { id = id });
        }

        public List<Post> getListPostAds()
        {
            var posts1 = db.Posts
            .Join(db.PostAdvertisements, post => post.IdPost, postad => postad.IdPost, (post, postad) => new { Post = post, PostAd = postad })
            .OrderBy(x => x.PostAd.views)
            .Take(5)
            .Select(x => x.Post)
            .ToList();

            foreach (var post in posts1)
            {
                var postads = db.PostAdvertisements.Where(x => x.IdPost == post.IdPost).First();
                post.PostAdvertisements.ToList()[0].views += 1;
                db.SaveChanges();
            }
            return posts1;
        }
        public List<Post> getListPostMostView()
        {
            var topPosts = db.Posts
                .GroupJoin(
                    db.Visits,
                    post => post.IdPost,
                    visit => visit.IdPost,
                    (post, visits) => new { Post = post, Visits = visits })
                .Select(pv => new { Post = pv.Post, Views = pv.Visits.Sum(v => v.Visit1) })
                .OrderByDescending(pv => pv.Views)
                .Select(pv => pv.Post)
                .Take(4)
                .ToList();
            return topPosts;
        }







        public string GenerateRandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var random = new Random();
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
