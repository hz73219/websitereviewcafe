﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using WebsiteReviewCafe.Models;
using WebsiteReviewCafe.Security;
using System.IO;
using System;

namespace WebsiteReviewCafe.Controllers
{
    public class ProfileController : BaseController
    {
        WebsiteReviewCafeContext db = new WebsiteReviewCafeContext();
        public IActionResult Index(string message)
        {
            int accountId = int.Parse(getDataToken());
            if (accountId > 0)
            {
                ModelProfile profile = new ModelProfile();
                int userId = db.Accounts.Find(accountId).UserId;
                profile.User = db.Users.Include(a=>a.Accounts.Where(a=>a.IdAccount==accountId)).Where(x=>x.UserId==userId).FirstOrDefault();
                profile.Post = db.Posts.Include(s=>s.Visits).Include(s=>s.PostAdvertisements).Where(post => post.UserId == userId && post.Status == true).ToList();
                var role = db.UpgradeAccounts.Where(x=>x.IdAccount == accountId).FirstOrDefault();
                if (role != null) { 
                    profile.ads = true;
                    profile.UpgradeAccount = role;
                }
                foreach (var post in profile.Post)
                {
                    profile.Shop.Add(db.Shops.Include(s => s.Menus).First(s => s.IdShop == post.IdShop));
                    var list = db.Evaluates
                          .Where(e => e.IdPost == post.IdPost)
                          .ToList();
                    if (list.Count > 0)
                    {
                        profile.Evaluate.Add(list.Average(p => p.Star));
                    }
                    {
                        profile.Evaluate.Add(0);
                    }
                }
                ViewBag.Message = message;

                return View(profile);

            }
            return BadRequest();

        }
        //upload ảnh
        [HttpPost]
        public IActionResult UploadImage(IFormFile imageFile)
        {
            int accountId = int.Parse(getDataToken());
            int userId = db.Accounts.Find(accountId).UserId;
            User user = db.Users.Find(userId);

            if (userId != 0 && imageFile != null && imageFile.Length > 0)
            {
                if (user.Image.Equals("/Images/user/avata.png") == false)
                {
                    var fileName = userId.ToString() + "user.png";
                    var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "images", "user", fileName);
                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        imageFile.CopyTo(stream);
                    }
                    user.Image = "/Images/user/" + fileName;
                    db.SaveChanges();
                }


            }
            return RedirectToAction("Index");
        }

        //Edit user information

        [HttpPost]
        public IActionResult editUser (User user)
        {
            int accountId = int.Parse(getDataToken());
            int userId = db.Accounts.Find(accountId).UserId;
            User userEdit = db.Users.Find(userId);
            if (accountId > 0 || user.FullName.Length == null || user.Email == null || user.PhoneNumber == null || user.Birthday == null)
            {
                userEdit.FullName = user.FullName;
                userEdit.Email = user.Email;
                userEdit.PhoneNumber = user.PhoneNumber;
                user.Birthday = user.Birthday;
                db.SaveChanges();
                return View("Index", userEdit);
            }
            return View("Index", userEdit);
        }
        // view thêm post
        public IActionResult addPost(string message)
        {
            int accountId = int.Parse(getDataToken());
            int userId = db.Accounts.Find(accountId).UserId;
            ViewBag.Message = message;
            return View();
        }
        // hàm thêm post
        [HttpPost]
        public IActionResult addNewPost(AddPost post,IFormFile image)
        {
            if (Validate(post, image) == false)
            {
                string message1 = "Đã xảy ra lỗi! Vui lòng thử lại!";
                return RedirectToAction("addPost", new { message = message1 });
            }
            int accountId = int.Parse(getDataToken());
            int userId = db.Accounts.Find(accountId).UserId;
            Post postAdd = new Post();
            Shop shopAdd = new Shop();
            shopAdd.NameShop = post.NameShop;
            shopAdd.NameShopOwner = post.NameShopOwner;
            shopAdd.DiaChi = post.DiaChi;
            shopAdd.OpenTime = post.OpenTime;
            shopAdd.ClosingTime = post.ClosingTime;
            shopAdd.PhoneShop = post.PhoneShop;
            db.Shops.Add(shopAdd);
            db.SaveChanges();
            int idShop = db.Shops.ToList().Last().IdShop;
            for (int i = 0; i < post.nameDrink.Count(); i++)
            {
                Menu menu = new Menu();
                menu.NameDrink = post.nameDrink[i];
                menu.Price = post.priceDrink[i];
                menu.IdShop = idShop;
                db.Menus.Add(menu);
                db.SaveChanges();
            }
            postAdd.Title = post.Title;
            postAdd.ContentPost = post.ContentPost;
            postAdd.IdTag = post.IdTag;
            postAdd.IdShop = idShop;
            postAdd.Date = DateTime.Now;
            postAdd.UserId = userId;
            postAdd.Status = false;
            if (image != null && image.Length > 0)
            {
                var fileName = GenerateRandomString(12) + ".png";
                var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "images", "post", fileName);
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    image.CopyTo(stream);
                }
                postAdd.Image = "/Images/post/" + fileName;

            }
            db.Posts.Add(postAdd);
            db.SaveChanges();
            User user = db.Users.Find(userId);
            string message = "Đã thêm post thành công! Vui lòng chờ duyệt bài!";
            return RedirectToAction("Index", new { message = message });
        }
        // hàm get tag cho post
        public IActionResult getTag()
        {
            int accountId = int.Parse(getDataToken());
            int userId = db.Accounts.Find(accountId).UserId;
            var lisst = db.Tags.ToList();
            return Json(lisst);
        }
        // hàm random
        public string GenerateRandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var random = new Random();
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        // hàm xác thực
        public bool Validate(AddPost post, IFormFile image)
        {
            try
            {
                if (post == null) { return false; }
                if (image == null) { return false; }
                if (post.priceDrink.Count() != post.nameDrink.Count() || post.nameDrink.Count() == 0) { return false; }
                if (post.Title == null) { return false; }
                if (post.ContentPost == null) { return false; }
                if (post.NameShop == null) { return false; }
                if (post.NameShopOwner == null) { return false; }
                if (post.DiaChi == null) { return false; }
                if (post.ClosingTime == null) { return false; }
                if (post.OpenTime == null) { return false; }
                if (post.PhoneShop == null) { return false; }
                if (post.IdTag == null) { return false; }
                return true;
            }
            catch
            {
                return false;
            }
        }
        // hàm edit post
        [HttpPost]
        public IActionResult editPost(int id, IFormCollection form,IFormFile file)
        {
            int accountId = int.Parse(getDataToken());
            int userId = db.Accounts.Find(accountId).UserId;
            Post postEdit = db.Posts.Find(id);
            if (postEdit.UserId != userId) { return BadRequest(); }
            Shop shopEdit = db.Shops.Find(postEdit.IdShop);
            shopEdit.NameShop = form["NameShop"];
            shopEdit.NameShopOwner = form["NameShopOwner"];
            shopEdit.DiaChi = form["DiaChi"];
            shopEdit.OpenTime = TimeSpan.Parse(form["OpenTime"]);
            shopEdit.ClosingTime = TimeSpan.Parse(form["ClosingTime"]);
            shopEdit.PhoneShop = form["PhoneShop"];
            db.SaveChanges();
            var listMenuDelete = db.Menus.Where(m => m.IdShop == shopEdit.IdShop).ToList();
            db.Menus.RemoveRange(listMenuDelete);
            db.SaveChanges();
            string[] nameDrink = form["nameDrink[]"];
            string[] priceDrink = form["priceDrink[]"];
            for (int i = 0; i < priceDrink.Count(); i++)
            {
                Menu menu = new Menu();
                menu.NameDrink = nameDrink[i];
                menu.Price = int.Parse(priceDrink[i]);
                menu.IdShop = shopEdit.IdShop;
                db.Menus.Add(menu);
                db.SaveChanges();
            }
            postEdit.Title = form["Title"];
            postEdit.ContentPost = form["ContentPost"];
            postEdit.IdTag = int.Parse(form["IdTag"]);
            postEdit.IdShop = shopEdit.IdShop;
            postEdit.Date = DateTime.Now;
            postEdit.UserId = userId;
            postEdit.Status = false;
            if (System.IO.File.Exists(postEdit.Image))
            {
                System.IO.File.Delete(postEdit.Image);
            }
            if (file != null && file.Length > 0)
            {
                var fileName = GenerateRandomString(12) + ".png";
                var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "images", "post", fileName);
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    file.CopyTo(stream);
                }
                postEdit.Image = "/Images/post/" + fileName;
            }

            db.SaveChanges();
            string message = "Đã thêm post thành công! Vui lòng chờ duyệt bài!";
            return RedirectToAction("Index", new { message = message });
        }

        public IActionResult adsPost(int id)
        {
            int accountId = int.Parse(getDataToken());
            Account account = db.Accounts.Find(accountId);
             
            if (account != null && account.Role.Equals("user"))
            {

                int so = db.PostAdvertisements.Where(x=>x.IdUser == account.UserId).ToList().Count();
                if (so > 4) { return Ok("Rất tiếc, mỗi tài khoản chỉ được quảng cáo tối đa 5 bài viết!"); }
                PostAdvertisement postAds = new PostAdvertisement();
                Post post = db.Posts.Find(id);
                postAds.IdPost = id;
                postAds.IdUser = post.UserId;
                db.PostAdvertisements.Add(postAds);
                db.SaveChanges();
                return Ok("Đăng bài quảng cáo thành công!");
            }
            return BadRequest("Lỗi");
        }
        [HttpPost]
        public IActionResult HadsPost(int id)
        {
            int accountId = int.Parse(getDataToken());
            Account account = db.Accounts.Find(accountId);
            if (account != null && account.Role.Equals("user"))
            {
                PostAdvertisement postAds = db.PostAdvertisements.Where(p => p.IdPost == id).ToList().Last();
                db.PostAdvertisements.Remove(postAds);
                db.SaveChanges();
                return Ok("Hủy quảng cáo thành công!");
            }
            return BadRequest("Lỗi");
        }

    }
}
