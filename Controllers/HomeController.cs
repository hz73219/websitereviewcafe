﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using Microsoft.Identity.Client;
using System.Diagnostics;
using System.Linq;
using WebsiteReviewCafe.Models;

namespace WebsiteReviewCafe.Controllers
{
    public class HomeController : BaseController
    {
        WebsiteReviewCafeContext db = new WebsiteReviewCafeContext();

        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            int accountId = int.Parse(getDataToken());
            Account account = db.Accounts.Find(accountId);
            if (account != null)
            {
                ModelHome home = new ModelHome();
                home.listPostPin = db.Posts.Include(x=>x.PostPins).Where(x =>x.PostPins.Count>0).ToList();
                home.listPostNew = db.Posts.Where(x=>x.Status==true).OrderByDescending(x=>x.Date).Take(5).ToList();
                home.listPostTrending = getListPostTrending();
                home.listPostADS = getListPostAds();
                home.listTag = db.Tags.ToList();
                home.listPostMostView = getListPostMostView();
                return View(home);

            }
            return RedirectToAction("Index", "Login", new { });

        }

        public IActionResult New(int page)
        {
            int accountId = int.Parse(getDataToken());
            Account account = db.Accounts.Find(accountId);
            if (account != null)
            {

                ModelHome home = new ModelHome();
                var listPost = db.Posts.OrderByDescending(x => x.Date).ToList();

                // Tính toán các thông số phân trang
                int pageSize = 5;// Số lượng sản phẩm trên một trang// Số lượng sản phẩm trên một trang
                int pageNumber = 1;
              
                int totalItems = listPost.Count; // Tổng số sản phẩm

                int totalPages = (int)Math.Ceiling((decimal)totalItems / pageSize); // Tổng số trang
                if (page != 0) pageNumber = page; // Số trang hiện tại
                if (page > totalPages) pageNumber --;
                int startPage = 1; // Trang bắt đầu hiển thị
                int endPage = totalPages; // Trang kết thúc hiển thị
                home.listPostNew = listPost.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
                ViewBag.startPage = startPage;
                ViewBag.endPage = endPage;
                ViewBag.pageNumber = pageNumber;
                home.listTag = db.Tags.ToList();
                home.listPostMostView = getListPostMostView();
                home.listPostTrending = getListPostAds();
                return View(home);
            }
            return RedirectToAction("Index", "Login", new { });
        }
        
        public IActionResult TypeCafe(int type,int page)
        {
            int accountId = int.Parse(getDataToken());
            Account account = db.Accounts.Find(accountId);
            if (account != null)
            {
                if(type==0)
                {
                    return NotFound();
                }
                ModelHome home = new ModelHome();
                var listPost = db.Posts.Where(x=>x.IdTag==type).ToList();
                if(listPost.Count == 0 ) {
                    home.listTag = db.Tags.ToList();
                    home.listPostMostView = getListPostMostView();
                    home.listPostTrending = getListPostAds();
                    home.message = "Rất tiếc không tìm thấy kết quả phù hợp";
                    return View(home);
                }
                // Tính toán các thông số phân trang
                int pageSize = 5;// Số lượng sản phẩm trên một trang// Số lượng sản phẩm trên một trang
                int pageNumber = 1;

                int totalItems = listPost.Count; // Tổng số sản phẩm

                int totalPages = (int)Math.Ceiling((decimal)totalItems / pageSize); // Tổng số trang
                if (page != 0) pageNumber = page; // Số trang hiện tại
                if (page > totalPages) pageNumber--;
                int startPage = 1; // Trang bắt đầu hiển thị
                int endPage = totalPages; // Trang kết thúc hiển thị
                home.listPost = listPost.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
                ViewBag.startPage = startPage;
                ViewBag.endPage = endPage;
                ViewBag.pageNumber = pageNumber;
                home.listTag = db.Tags.ToList();
                home.listPostMostView = getListPostMostView();
                home.listPostTrending = getListPostAds();
                home.message = "Tìm kiếm theo:" + db.Tags.Find(type).NameTag;
                return View(home);
            }
            return RedirectToAction("Index", "Login", new { });
        }

        public IActionResult Trending(int page) {
            int accountId = int.Parse(getDataToken());
            Account account = db.Accounts.Find(accountId);
            if (account != null)
            {

                ModelHome home = new ModelHome();
                var listPost = getTrending();

                // Tính toán các thông số phân trang
                int pageSize = 5;// Số lượng sản phẩm trên một trang// Số lượng sản phẩm trên một trang
                int pageNumber = 1;

                int totalItems = listPost.Count; // Tổng số sản phẩm

                int totalPages = (int)Math.Ceiling((decimal)totalItems / pageSize); // Tổng số trang
                if (page != 0) pageNumber = page; // Số trang hiện tại
                if (page > totalPages) pageNumber--;
                int startPage = 1; // Trang bắt đầu hiển thị
                int endPage = totalPages; // Trang kết thúc hiển thị
                home.listPostNew = listPost.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
                ViewBag.startPage = startPage;
                ViewBag.endPage = endPage;
                ViewBag.pageNumber = pageNumber;
                home.listTag = db.Tags.ToList();
                home.listPostMostView = getListPostMostView();
                home.listPostTrending = getListPostAds();
                return View(home);
            }
            return RedirectToAction("Index", "Login", new { });
        }

        public IActionResult Search(string search, int page)
        {
            int accountId = int.Parse(getDataToken());
            Account account = db.Accounts.Find(accountId);
            if (account != null)
            {
                if (search == "")
                {
                    return NotFound();
                }
                ModelHome home = new ModelHome();
                var listPost = db.Posts
                    .Join(db.Shops,
                        post => post.IdShop,
                        shop => shop.IdShop,
                        (post, shop) => new { Post = post, Shop = shop })
                    .Where(joinResult => joinResult.Shop.NameShop.Contains(search) || joinResult.Shop.DiaChi.Contains(search))
                    .Select(joinResult => joinResult.Post)
                    .ToList();
                if (listPost.Count == 0)
                {
                    home.listTag = db.Tags.ToList();
                    home.listPostMostView = getListPostMostView();
                    home.listPostTrending = getListPostAds();
                    home.message = "Rất tiếc không tìm thấy kết quả phù hợp";
                    return View(home);
                }
                // Tính toán các thông số phân trang
                int pageSize = 5;// Số lượng sản phẩm trên một trang// Số lượng sản phẩm trên một trang
                int pageNumber = 1;

                int totalItems = listPost.Count; // Tổng số sản phẩm

                int totalPages = (int)Math.Ceiling((decimal)totalItems / pageSize); // Tổng số trang
                if (page != 0) pageNumber = page; // Số trang hiện tại
                if (page > totalPages) pageNumber--;
                int startPage = 1; // Trang bắt đầu hiển thị
                int endPage = totalPages; // Trang kết thúc hiển thị
                home.listPost = listPost.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
                ViewBag.startPage = startPage;
                ViewBag.endPage = endPage;
                ViewBag.pageNumber = pageNumber;
                home.listTag = db.Tags.ToList();
                home.listPostMostView = getListPostMostView();
                home.listPostTrending = getListPostAds();
                home.message = "Tìm kiếm theo:" + search;
                return View(home);
            }
            return RedirectToAction("Index", "Login", new { });
        }
        public IActionResult MostViews(int page)
        {
            int accountId = int.Parse(getDataToken());
            Account account = db.Accounts.Find(accountId);
            if (account != null)
            {

                ModelHome home = new ModelHome();
                var listPost = db.Posts.OrderByDescending(x => x.Date).ToList();

                // Tính toán các thông số phân trang
                int pageSize = 5;// Số lượng sản phẩm trên một trang// Số lượng sản phẩm trên một trang
                int pageNumber = 1;

                int totalItems = listPost.Count; // Tổng số sản phẩm

                int totalPages = (int)Math.Ceiling((decimal)totalItems / pageSize); // Tổng số trang
                if (page != 0) pageNumber = page; // Số trang hiện tại
                if (page > totalPages) pageNumber--;
                int startPage = 1; // Trang bắt đầu hiển thị
                int endPage = totalPages; // Trang kết thúc hiển thị
                home.listPostNew = listPost.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
                ViewBag.startPage = startPage;
                ViewBag.endPage = endPage;
                ViewBag.pageNumber = pageNumber;
                home.listTag = db.Tags.ToList();
                home.listPostMostView = getListPostMostView();
                home.listPostTrending = getListPostAds();
                return View(home);
            }
            return RedirectToAction("Index", "Login", new { });
        }

        public List<Post> getListPostAds()
        {
            var posts1 = db.Posts
            .Join(db.PostAdvertisements, post => post.IdPost, postad => postad.IdPost, (post, postad) => new { Post = post, PostAd = postad })
            .OrderBy(x => x.PostAd.views)
            .Take(5)
            .Select(x => x.Post)
            .ToList();
           
            foreach (var post in posts1)
            {
                var postads = db.PostAdvertisements.Where(x => x.IdPost == post.IdPost).First();
                post.PostAdvertisements.ToList()[0].views += 1;
                db.SaveChanges();
            }
            return posts1;
        }
        public List<Post> getListPostTrending()
        {
            DateTime startDate = DateTime.Today.AddDays(-7);
            var mostViewedPosts = db.Posts
                .Join(db.Visits,
                    post => post.IdPost,
                    truyCap => truyCap.IdPost,
                    (post, truyCap) => new { post, truyCap })
                .Where(x => x.truyCap.date >= startDate)
                .GroupBy(x => x.post)
                .Select(g => new
                {
                    Post = g.Key,
                    TongLuotTruyCap = g.Sum(x => x.truyCap.Visit1)
                })
                .OrderByDescending(x => x.TongLuotTruyCap)
                .Select(x=>x.Post)
                .Take(6)
                .ToList();
            return mostViewedPosts;
        }
        public List<Post> getListPostMostView()
        {
            var topPosts = db.Posts
                .GroupJoin(
                    db.Visits,
                    post => post.IdPost,
                    visit => visit.IdPost,
                    (post, visits) => new { Post = post, Visits = visits })
                .Select(pv => new { Post = pv.Post, Views = pv.Visits.Sum(v => v.Visit1) })
                .OrderByDescending(pv => pv.Views)
                .Select(pv => pv.Post)
                .Take (4)
                .ToList();
            return topPosts;
        }

        public List<Post> getListPostMostViewLayout()
        {
            var topPosts = db.Posts
                .GroupJoin(
                    db.Visits,
                    post => post.IdPost,
                    visit => visit.IdPost,
                    (post, visits) => new { Post = post, Visits = visits })
                .Select(pv => new { Post = pv.Post, Views = pv.Visits.Sum(v => v.Visit1) })
                .OrderByDescending(pv => pv.Views)
                .Select(pv => pv.Post)
                .Take(9)
                .ToList();
            return topPosts;
        }
        public List<Tag> getListTag()
        {
            return db.Tags.ToList();
        }

        public List<Post> getTrending()
        {
            DateTime startDate = DateTime.Today.AddDays(-7);
            var mostViewedPosts = db.Posts
                .Join(db.Visits,
                    post => post.IdPost,
                    truyCap => truyCap.IdPost,
                    (post, truyCap) => new { post, truyCap })
                .Where(x => x.truyCap.date >= startDate)
                .GroupBy(x => x.post)
                .Select(g => new
                {
                    Post = g.Key,
                    TongLuotTruyCap = g.Sum(x => x.truyCap.Visit1)
                })
                .OrderByDescending(x => x.TongLuotTruyCap)
                .Select(x => x.Post)
                .ToList();
            return mostViewedPosts;
        }

          
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}