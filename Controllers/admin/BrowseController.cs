﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebsiteReviewCafe.Models;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace WebsiteReviewCafe.Controllers.admin
{
    [Route("admin/browse/{action=index}")]
    public class BrowseController : BaseController
    {
        WebsiteReviewCafeContext db = new WebsiteReviewCafeContext();
        public IActionResult Index()
        {
            int accountId = int.Parse(getDataToken());
            Account account = db.Accounts.Find(accountId);
            if (account != null && account.Role.Equals("admin"))
            {
                User user = db.Users.Find(account.UserId);
                ViewBag.User = user;

                List<Post> listPost = db.Posts.Where(p => p.Status == false).ToList();
                List<Shop> listShop = new List<Shop>();
                List<Tag> listTag = new List<Tag>();
                foreach (var post in listPost)
                {
                    listShop.Add(db.Shops.Include(s => s.Menus).First(s => s.IdShop == post.IdShop));
                    listTag.Add(db.Tags.First(t => t.IdTag == post.IdTag));
                }
                ViewBag.Shop = listShop;
                ViewBag.Tag = listTag;
                return View(listPost);
            }
            return RedirectToAction("Index", "Login", new { });
        }
        [HttpPost]
        public IActionResult deletePost(int idPost)
        {
            int accountId = int.Parse(getDataToken());
            Account account = db.Accounts.Find(accountId);
            if (account != null && account.Role.Equals("admin"))
            {
                Post postDelete = db.Posts.Find(idPost);
                db.Posts.Remove(postDelete);
                db.SaveChanges();
                return Ok("Xóa thành công!");
            }
            return BadRequest();

        }
        [HttpPost]
        public IActionResult accepPost(int idPost)
        {
            int accountId = int.Parse(getDataToken());
            Account account = db.Accounts.Find(accountId);
            if (account != null && account.Role.Equals("admin"))
            {
                Post postAcp = db.Posts.Find(idPost);
                postAcp.Status = true;
                db.SaveChanges();
                return Ok("Phê duyệt thành công!");
            }
            return BadRequest();
        }
    }
}
