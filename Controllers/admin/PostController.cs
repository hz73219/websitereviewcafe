﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebsiteReviewCafe.Models;

namespace WebsiteReviewCafe.Controllers.admin
{
    [Route("admin/post/{action=index}")]
    public class PostController : BaseController
    {
        WebsiteReviewCafeContext db = new WebsiteReviewCafeContext();
        public IActionResult Index()
        {
            int accountId = int.Parse(getDataToken());
            Account account = db.Accounts.Find(accountId);
            if (account != null && account.Role.Equals("admin"))
            {
                User user = db.Users.Find(account.UserId);
                ViewBag.User = user;
                List<Post> listPost = db.Posts.Include(p => p.PostPins).Include(p => p.PostAdvertisements).Where(p => p.Status == true).ToList();
                List<Shop> listShop = new List<Shop>();
                List<Tag> listTag = new List<Tag>();
                foreach (var post in listPost)
                {
                    listShop.Add(db.Shops.Include(s => s.Menus).First(s => s.IdShop == post.IdShop));
                    listTag.Add(db.Tags.First(t => t.IdTag == post.IdTag));
                }
                ViewBag.Shop = listShop;
                ViewBag.Tag = listTag;
                return View(listPost);
            }
            return RedirectToAction("Index", "Login", new { });
        }
        [HttpPost]
        public IActionResult pinPost(int id)
        {
            int accountId = int.Parse(getDataToken());
            Account account = db.Accounts.Find(accountId);
            if (account != null && account.Role.Equals("admin"))
            {
                PostPin postPin = new PostPin();
                postPin.IdPost = id;
                db.PostPins.Add(postPin);
                db.SaveChanges();
                return Ok("Ghim thành công!");
            }
            return BadRequest();
        }
        [HttpPost]
        public IActionResult HpinPost(int id)
        {
            int accountId = int.Parse(getDataToken());
            Account account = db.Accounts.Find(accountId);
            if (account != null && account.Role.Equals("admin"))
            {
                PostPin postPinList = db.PostPins.Where(p => p.IdPost == id).ToList().Last();
                db.PostPins.Remove(postPinList);
                db.SaveChanges();
                return Ok("Hủy ghim thành công!");
            }
            return BadRequest();
        }
        [HttpPost]
        public IActionResult adsPost(int id)
        {
            int accountId = int.Parse(getDataToken());
            Account account = db.Accounts.Find(accountId);
            if (account != null && account.Role.Equals("admin"))
            {
                PostAdvertisement postAds = new PostAdvertisement();
                Post post = db.Posts.Find(id);
                postAds.IdPost = id;
                postAds.IdUser = post.UserId;
                db.PostAdvertisements.Add(postAds);
                db.SaveChanges();
                return Ok("Đăng bài quảng cáo thành công!");
            }
            return BadRequest("Lỗi");
        }
        [HttpPost]
        public IActionResult HadsPost(int id)
        {
            int accountId = int.Parse(getDataToken());
            Account account = db.Accounts.Find(accountId);
            if (account != null && account.Role.Equals("admin"))
            {
                PostAdvertisement postAds = db.PostAdvertisements.Where(p => p.IdPost == id).ToList().Last();
                db.PostAdvertisements.Remove(postAds);
                db.SaveChanges();
                return Ok("Hủy quảng cáo thành công!");
            }
            return BadRequest("Lỗi");
        }
    }
}

