﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebsiteReviewCafe.Models;

namespace WebsiteReviewCafe.Controllers.admin
{
    [Route("admin/updateAccount/{action=index}")]
    public class UpdateAccountController : BaseController
    {
        WebsiteReviewCafeContext db = new WebsiteReviewCafeContext();
        public IActionResult Index()
        {

            try
            {
                int accountId = int.Parse(getDataToken());
                Account account = db.Accounts.Find(accountId);
                if (account != null && account.Role.Equals("admin"))
                {
                    User user = db.Users.Find(account.UserId);
                    ViewBag.User = user;
                    List<Account> listAccount = db.Accounts.Include(p=>p.UpgradeAccounts).Where(p => p.Role == "user").ToList();
                    return View(listAccount);
                }
                return RedirectToAction("Index", "Login", new { });
            }
            catch
            {
                return RedirectToAction("Index", "Login", new { });
            }
        }
        [HttpPost]
        public IActionResult update(int id)
        {
            try
            {
                int accountId = int.Parse(getDataToken());
                Account account = db.Accounts.Find(accountId);
                if (account != null && account.Role.Equals("admin"))
                {
                    UpgradeAccount account1 = new UpgradeAccount();
                    account1.IdAccount = id;
                    account1.TimeUpgrade = DateTime.Now.AddDays(30);
                    var accountUpdate = db.UpgradeAccounts.FirstOrDefault(u => u.IdAccount == id);
                    if (accountUpdate != null)
                    {
                        accountUpdate.TimeUpgrade = DateTime.Now.AddDays(30);
                        db.SaveChanges();
                    }
                    else
                    {
                        db.UpgradeAccounts.Add(account1 );
                        db.SaveChanges();
                    }
                  
                    return Ok("Nâng cấp thành công!");
                }
                return BadRequest();
            }
            catch
            {
                return BadRequest();
            }
        }
        [HttpPost]
        public IActionResult delete(int id)
        {
            try
            {
                int accountId = int.Parse(getDataToken());
                Account account = db.Accounts.Find(accountId);
                if (account != null && account.Role.Equals("admin"))
                {
                    Account accountDelete = db.Accounts.Find(id);
                    db.Accounts.Remove(accountDelete);
                    db.SaveChanges();
                    return Ok("Xóa thành công!");
                }
                return BadRequest();
            }
            catch
            {
                return BadRequest();
            }

        }

    }
}
