﻿using Microsoft.AspNetCore.Mvc;

using WebsiteReviewCafe.Models;
using WebsiteReviewCafe.Security;

namespace WebsiteReviewCafe.Controllers
{
    public class LoginController : BaseController
    {
        WebsiteReviewCafeContext db = new WebsiteReviewCafeContext();
        JwtFilter jwtFilter = new JwtFilter();
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult forget()
        {
            
            return View();
        }
        [HttpPost]
        public IActionResult register(Register user)
        {
            try
            {
                User userAdd = new User();
                userAdd.Email = user.Email;
                userAdd.FullName = user.FullName;
                userAdd.PhoneNumber = user.PhoneNumber;
                userAdd.Birthday = user.Birthday;
                userAdd.Image = "/Images/user/avata.png";
                db.Users.Add(userAdd);
                db.SaveChanges();
                int iduser = db.Users.ToList().Last().UserId;
                Account accountAdd = new Account();
                accountAdd.UserName = user.UserName;
                accountAdd.Password = jwtFilter.hashPassword(user.Password);
                accountAdd.UserId = iduser;
                accountAdd.Role = "user";
                db.Accounts.Add(accountAdd);
                db.SaveChanges();
                return Ok();
            }
            catch {
                return BadRequest();
                    }
           
        }
        [HttpPost]
        public IActionResult checkUsername(string username)
        {
            if (username == null)
            {
                return BadRequest();
            }
            var user = db.Accounts.FirstOrDefault(x => x.UserName == username);
            if (user == null)
            {
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        
        }
        [HttpPost]
        public IActionResult checkEmail(string email)
        {
            if (email == null)
            {
                return BadRequest();
            }
            var user = db.Users.FirstOrDefault(x => x.Email == email);
            if (user == null)
            {
                return Ok();
            }
            else
            {
                return BadRequest();
            }

        }
        public IActionResult Login(string username, string password)
        {
            try
            {
                var userLogin = db.Accounts.FirstOrDefault(u => u.UserName == username);
                if (userLogin == null)
                {

                    return RedirectToAction("Index");
                }
                else
                {
                    if (jwtFilter.verifyPassword(password, userLogin.Password))
                    {
                        if (userLogin.Role.Equals("admin"))
                        {
                            string token = jwtFilter.generateJwtToken(userLogin.IdAccount.ToString(), userLogin.Role);
                            var cookieOptions = new CookieOptions();
                            cookieOptions.Expires = DateTime.Now.AddDays(1);
                            cookieOptions.Path = "/";
                            Response.Cookies.Append("token", token, cookieOptions);
                            return RedirectToAction("Index", "browse", new { });
                        }
                        if (userLogin.Role.Equals("user"))
                        {
                            string token = jwtFilter.generateJwtToken(userLogin.IdAccount.ToString(), userLogin.Role);
                            var cookieOptions = new CookieOptions();
                            cookieOptions.Expires = DateTime.Now.AddDays(1);
                            cookieOptions.Path = "/";
                            Response.Cookies.Append("token", token, cookieOptions);
                            return RedirectToAction("Index", "Home", new { });
                        }

                    }
                    ViewBag.Messeage = "Không tìm thấy tài khoản hoặc mật khẩu";
                    return  RedirectToAction("Index", "Login", new { });
                }
            }
            catch
            {
                return RedirectToAction("Index", "Login", new { });
            }
        }
        [HttpPost]
        public IActionResult sendEmail(string email)
        {
            int userId = checkEmail1(email);
            if (userId!=0)
            {
                Validate validate = new Validate();
                string tokenPass = GenerateRandomString(10);
                validate.SendEmail(email, "Thay đổi mật khẩu:", tokenPass);
                Token tokenOld = db.Tokens.FirstOrDefault(x => x.UserId== userId);
                Token token = new Token();
                if (tokenOld!=null)
                {
                    db.Tokens.Remove(tokenOld);
                    db.SaveChanges();
                }
                token.UserId = userId;
                token.Token1 = tokenPass;
                token.DateExpired = DateTime.Now.AddDays(1);
                db.Tokens.Add(token);
                db.SaveChanges();
                return Ok();
            }
            return BadRequest();
        }
        public string GenerateRandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var random = new Random();
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        public int checkEmail1(string email)
        {
            if (email == null)
            {
                return 0;
            }
            var user = db.Users.FirstOrDefault(x => x.Email == email);
            if (user != null)
            {
                return user.UserId;
            }
            else
            {
                return 0;
            }

        }

        public IActionResult newPassword(string token,string newPass)
        {
            try
            {
               Token tokenFind = db.Tokens.FirstOrDefault(x => x.Token1.Equals(token));
                if (tokenFind != null)
                {
                    if (!string.IsNullOrEmpty(newPass))
                    {                   
                        Account account = db.Accounts.FirstOrDefault(x => x.UserId == tokenFind.UserId);
                        account.Password = jwtFilter.hashPassword(newPass);
                        db.SaveChanges();
                        ViewBag.check = 1;
                        ViewBag.thongBao = "Đổi mật khẩu thành công vui lòng đăng nhập";
                        db.Tokens.Remove(tokenFind);
                        db.SaveChanges(true);
                        return View();
                    }
                    ViewBag.token = token;
                    ViewBag.check = 0;
                    return View();
                }
                return BadRequest();
            }
            catch
            {
                return BadRequest();
            }
        }
        public IActionResult logOut()
        {
            Response.Cookies.Delete("token");
            return RedirectToAction("Index", "Login", new { });
        }
    }
}
