﻿using Microsoft.AspNetCore.Mvc;
using WebsiteReviewCafe.Models;
using WebsiteReviewCafe.Security;

namespace WebsiteReviewCafe.Controllers
{
    public class BaseController : Controller
    {  
            public string getDataToken()
            {
                string token = Request.Cookies["token"];
                JwtFilter filter = new JwtFilter();
                if (token != null)
                {
                    return filter.decryptionJwtToken(token).IdAccount;
                }
                else
                {
                    return "0";
                }
            }
    
    }
}
