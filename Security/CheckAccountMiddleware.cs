﻿using Microsoft.EntityFrameworkCore;
using WebsiteReviewCafe.Models;

namespace WebsiteReviewCafe.Security
{
    public class CheckAccountMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<CookieMiddleware> _logger;

        public CheckAccountMiddleware(RequestDelegate next, ILogger<CookieMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }
        public async Task Invoke(HttpContext context)
        {
            WebsiteReviewCafeContext db = new WebsiteReviewCafeContext();
            List<Account> accounts = db.Accounts
                                    .Join(db.UpgradeAccounts,
                                        acc => acc.IdAccount,
                                        updAcc => updAcc.IdAccount,
                                        (acc, updAcc) => new { Account = acc, UpdateAccount = updAcc })
                                    .Where(x => x.UpdateAccount.TimeUpgrade < DateTime.UtcNow)
                                    .Select(x => x.Account)
                                    .ToList();
            List<UpgradeAccount> update = db.UpgradeAccounts.Where(x=>x.TimeUpgrade < DateTime.UtcNow).ToList();
            if (accounts.Count == 0) { await _next(context);
                return;
            }
            else {
                foreach (var account in accounts)
                {
                    var adsToDelete = db.PostAdvertisements
                    .Where(ad => ad.IdUser == account.UserId)
                    .ToList();
                    db.PostAdvertisements.RemoveRange(adsToDelete);
                    db.SaveChanges();
                }
                db.UpgradeAccounts.RemoveRange(update);
                db.SaveChanges();
                await _next(context);
                return;
            }
        }
    }
}
