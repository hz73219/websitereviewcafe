﻿using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using WebsiteReviewCafe.Models;

namespace WebsiteReviewCafe.Security
{
    public class JwtFilter : Configure
    {
        // mã hóa mật khẩu
        public string hashPassword(string password)
        {
            if (password != null)
            {
                var saltBytes = Encoding.ASCII.GetBytes(Key);
                var passwordBytes = Encoding.ASCII.GetBytes(password);
                // Tạo khóa bí mật
                var hmac = new HMACSHA256(saltBytes);
                // Tạo mã băm
                var hash = hmac.ComputeHash(passwordBytes);
                // Chuyển đổi mã băm thành chuỗi hex
                var hexHash = BitConverter.ToString(hash).Replace("-", "");
                return hexHash;
            }
            return "";

        }
        //Xác thực mật khẩu
        public bool verifyPassword(string password, string hashedPassword)
        {
            if (password != null)
            {
                string passwordInput = hashPassword(password);
                if (passwordInput == hashedPassword)
                {
                    return true;
                }
                else
                { return false; }
            }
            return false;
        }
        //Tạo chuỗi token
        public string generateJwtToken(string accountId,string role)
        {
            var SecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Key));

            // Tạo các thông tin cho token
            var claims = new[] {
                new Claim(JwtRegisteredClaimNames.Name, accountId),
                new Claim(JwtRegisteredClaimNames.Typ, role)
            };
            var expires = DateTime.UtcNow.AddDays(1);
            var signingCredentials = new SigningCredentials(SecurityKey, SecurityAlgorithms.HmacSha256);
            // Tạo token
            var token = new JwtSecurityToken(
                issuer: Issuser,
                audience: Audience,
                claims: claims,
                expires: expires,
                signingCredentials: signingCredentials);
            // Mã hóa token thành chuỗi
            var tokenString = new JwtSecurityTokenHandler().WriteToken(token);
            return tokenString;
        }
        //giải mã chuỗi token
        public IdRole decryptionJwtToken(string tokenString)
        {
            // Chuỗi key bí mật được sử dụng để mã hóa token
            var SecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Key));

            // Thiết lập các thông tin cho xác thực token
            var validationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = Issuser,
                ValidateAudience = true,
                ValidAudience = Audience,
                ValidateLifetime = true,
                IssuerSigningKey = SecurityKey
            };
            // Giải mã token
            var handler = new JwtSecurityTokenHandler();
            ClaimsPrincipal user = handler.ValidateToken(tokenString, validationParameters, out var validatedToken);
            // Lấy thông tin user và role từ token
            var accountId = user.FindFirst(JwtRegisteredClaimNames.Name)?.Value;
            var role = user.FindFirst(JwtRegisteredClaimNames.Typ)?.Value;
            if (accountId != null&&role!=null)
            {
                IdRole idRole = new IdRole(accountId,role);
                return idRole;

            }
            else
            {
                IdRole idRole = new IdRole();
                return idRole;
            }

        }
    }
}
