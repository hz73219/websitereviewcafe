﻿namespace WebsiteReviewCafe.Security
{
    public class Configure
    {
        protected string Issuser { get; set; }
        protected string Audience { get; set; }
        protected string Key { get; set; }

        public Configure()
        {
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            var config = builder.Build();
            var section = config.GetSection("Jwt");

            this.Issuser = section["Issuser"];
            this.Audience = section["Audience"];
            this.Key = section["Key"];

        }
    }
}
