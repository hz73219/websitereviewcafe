﻿namespace WebsiteReviewCafe.Security
{
    public class IdRole
    {
        public IdRole(string IdAccount,string Role) { 
            this.IdAccount = IdAccount;
            this.Role = Role;
        }
        public IdRole()
        {
            this.IdAccount = "0";
            this.Role = "0";
        }
        public string IdAccount { get; set; }
        public string Role { get; set; }
    }
}
